"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChargeInput = exports.CreateInput = exports.CreateReturn = exports.PaydunyaPaymentIdInput = exports.CreditAccountIdInput = exports.CreditAccountInput = exports.ItemType = void 0;
const graphql_1 = require("@nestjs/graphql");
const class_validator_1 = require("class-validator");
let ItemType = class ItemType {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], ItemType.prototype, "name", void 0);
__decorate([
    graphql_1.Field(() => Number),
    __metadata("design:type", Number)
], ItemType.prototype, "quantity", void 0);
__decorate([
    graphql_1.Field(() => Number),
    __metadata("design:type", Number)
], ItemType.prototype, "unitPrice", void 0);
__decorate([
    graphql_1.Field({ nullable: true }),
    __metadata("design:type", String)
], ItemType.prototype, "description", void 0);
ItemType = __decorate([
    graphql_1.InputType()
], ItemType);
exports.ItemType = ItemType;
let CreditAccountInput = class CreditAccountInput {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreditAccountInput.prototype, "customer", void 0);
__decorate([
    graphql_1.Field(() => Number),
    __metadata("design:type", Number)
], CreditAccountInput.prototype, "amount", void 0);
CreditAccountInput = __decorate([
    graphql_1.InputType()
], CreditAccountInput);
exports.CreditAccountInput = CreditAccountInput;
class CreditAccountIdInput {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsUUID(),
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreditAccountIdInput.prototype, "id", void 0);
exports.CreditAccountIdInput = CreditAccountIdInput;
class PaydunyaPaymentIdInput {
}
__decorate([
    class_validator_1.IsNotEmpty(),
    class_validator_1.IsUUID(),
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], PaydunyaPaymentIdInput.prototype, "id", void 0);
exports.PaydunyaPaymentIdInput = PaydunyaPaymentIdInput;
let CreateReturn = class CreateReturn {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateReturn.prototype, "oprToken", void 0);
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateReturn.prototype, "token", void 0);
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateReturn.prototype, "responseText", void 0);
CreateReturn = __decorate([
    graphql_1.ObjectType()
], CreateReturn);
exports.CreateReturn = CreateReturn;
let CreateInput = class CreateInput {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], CreateInput.prototype, "customer", void 0);
__decorate([
    graphql_1.Field(() => [ItemType]),
    __metadata("design:type", Array)
], CreateInput.prototype, "items", void 0);
CreateInput = __decorate([
    graphql_1.InputType()
], CreateInput);
exports.CreateInput = CreateInput;
let ChargeInput = class ChargeInput {
};
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], ChargeInput.prototype, "oprToken", void 0);
__decorate([
    graphql_1.Field(() => String),
    __metadata("design:type", String)
], ChargeInput.prototype, "code", void 0);
ChargeInput = __decorate([
    graphql_1.InputType()
], ChargeInput);
exports.ChargeInput = ChargeInput;
//# sourceMappingURL=dto.js.map