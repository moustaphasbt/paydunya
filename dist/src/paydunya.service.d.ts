import { Repository } from 'typeorm';
import { ItemType, CreateInput, CreateReturn, ChargeInput, CreditAccountInput } from './dto';
import { CreditAccount } from './entity/credit.account.entity';
import { PaydunyaPayment } from './entity/paydunya.payment.entity';
import { PaydunyaOptions } from './utils/common';
export declare class PaydunyaService {
    private creditAccountRepositpty;
    private paydunyaPaymentRepositpty;
    private options;
    private invoice;
    private directPay;
    private checkout;
    constructor(creditAccountRepositpty: Repository<CreditAccount>, paydunyaPaymentRepositpty: Repository<PaydunyaPayment>, options: PaydunyaOptions);
    addItem({ name, quantity, unitPrice, description }: ItemType): void;
    create({ customer, items }: CreateInput): Promise<CreateReturn>;
    charge({ code, oprToken }: ChargeInput): Promise<PaydunyaPayment>;
    checkoutInvoice(token: string): Promise<any>;
    creditAccount({ customer, amount, }: CreditAccountInput): Promise<CreditAccount>;
}
