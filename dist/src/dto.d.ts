export declare class ItemType {
    name: string;
    quantity: number;
    unitPrice: number;
    description?: string;
}
export declare class CreditAccountInput {
    customer: string;
    amount: number;
}
export declare class CreditAccountIdInput {
    id: string;
}
export declare class PaydunyaPaymentIdInput {
    id: string;
}
export declare class CreateReturn {
    oprToken: string;
    token: string;
    responseText: string;
}
export declare class CreateInput {
    customer: string;
    items: [ItemType];
}
export declare class ChargeInput {
    oprToken: string;
    code: string;
}
