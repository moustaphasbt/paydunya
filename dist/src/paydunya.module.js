"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var PaydunyaModule_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaydunyaModule = void 0;
const common_1 = require("@nestjs/common");
const paydunya_resolver_1 = require("./paydunya.resolver");
const paydunya_service_1 = require("./paydunya.service");
const common_2 = require("./utils/common");
let PaydunyaModule = PaydunyaModule_1 = class PaydunyaModule {
    static forRoot({ imports, inject, useFactory }) {
        return {
            imports: imports,
            module: PaydunyaModule_1,
            providers: [
                paydunya_resolver_1.PaydunyaResolver,
                paydunya_service_1.PaydunyaService,
                {
                    provide: common_2.PAYDUNYA_CONFIG,
                    useFactory,
                    inject,
                },
            ],
            exports: [paydunya_service_1.PaydunyaService],
        };
    }
};
PaydunyaModule = PaydunyaModule_1 = __decorate([
    common_1.Module({})
], PaydunyaModule);
exports.PaydunyaModule = PaydunyaModule;
//# sourceMappingURL=paydunya.module.js.map