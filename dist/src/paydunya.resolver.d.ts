import { CreateReturn, CreateInput, ChargeInput, CreditAccountInput } from './dto';
import { CreditAccount } from './entity/credit.account.entity';
import { PaydunyaPayment } from './entity/paydunya.payment.entity';
import { PaydunyaService } from './paydunya.service';
export declare class PaydunyaResolver {
    private readonly paydunyaService;
    constructor(paydunyaService: PaydunyaService);
    create(input: CreateInput): Promise<CreateReturn>;
    charge(input: ChargeInput): Promise<PaydunyaPayment>;
    checkoutInvoice(token: string): Promise<any>;
    creditAccount(input: CreditAccountInput): Promise<CreditAccount>;
}
