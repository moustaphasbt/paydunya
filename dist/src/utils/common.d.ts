export interface Options {
    useFactory: (config: any) => PaydunyaOptions;
    inject: any[];
    imports: any[];
}
export interface PaydunyaOptions {
    PAYDUNYA_MASTER_KEY: string;
    PAYDUNYA_PRIVATE_KEY: string;
    PAYDUNYA_PUBLIC_KEY: string;
    PAYDUNYA_TOKEN: string;
    PAYDUNYA_MODE: string;
    PAYDUNYA_NAME: string;
}
export declare const PAYDUNYA_CONFIG = "paydunya-config";
