export declare class CreditAccount {
    id: string;
    description: string;
    responseText: string;
    transactionID: string;
    createdAt: Date;
    updatedAt: Date;
}
