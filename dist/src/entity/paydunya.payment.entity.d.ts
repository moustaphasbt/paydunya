export declare class PaydunyaPayment {
    id: string;
    status: string;
    responseText: string;
    receiptURL: string;
    createdAt: Date;
    updatedAt: Date;
}
