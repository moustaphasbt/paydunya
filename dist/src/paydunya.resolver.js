"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaydunyaResolver = void 0;
const common_1 = require("@nestjs/common");
const graphql_1 = require("@nestjs/graphql");
const dto_1 = require("./dto");
const credit_account_entity_1 = require("./entity/credit.account.entity");
const paydunya_payment_entity_1 = require("./entity/paydunya.payment.entity");
const paydunya_service_1 = require("./paydunya.service");
let PaydunyaResolver = class PaydunyaResolver {
    constructor(paydunyaService) {
        this.paydunyaService = paydunyaService;
    }
    async create(input) {
        return await this.paydunyaService.create(input);
    }
    async charge(input) {
        return await this.paydunyaService.charge(input);
    }
    async checkoutInvoice(token) {
        return await this.paydunyaService.checkoutInvoice(token);
    }
    async creditAccount(input) {
        return await this.paydunyaService.creditAccount(input);
    }
};
__decorate([
    graphql_1.Mutation(() => dto_1.CreateReturn),
    __param(0, graphql_1.Args('input', { type: () => dto_1.CreateInput })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreateInput]),
    __metadata("design:returntype", Promise)
], PaydunyaResolver.prototype, "create", null);
__decorate([
    graphql_1.Mutation(() => paydunya_payment_entity_1.PaydunyaPayment),
    __param(0, graphql_1.Args('input', { type: () => dto_1.ChargeInput })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.ChargeInput]),
    __metadata("design:returntype", Promise)
], PaydunyaResolver.prototype, "charge", null);
__decorate([
    graphql_1.Query(() => String),
    __param(0, graphql_1.Args('token', { type: () => String })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], PaydunyaResolver.prototype, "checkoutInvoice", null);
__decorate([
    graphql_1.Mutation(() => credit_account_entity_1.CreditAccount),
    __param(0, graphql_1.Args('input', { type: () => dto_1.CreditAccountInput })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [dto_1.CreditAccountInput]),
    __metadata("design:returntype", Promise)
], PaydunyaResolver.prototype, "creditAccount", null);
PaydunyaResolver = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [paydunya_service_1.PaydunyaService])
], PaydunyaResolver);
exports.PaydunyaResolver = PaydunyaResolver;
//# sourceMappingURL=paydunya.resolver.js.map