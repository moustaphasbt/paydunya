"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PaydunyaService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const paydunya = require("paydunya");
const typeorm_2 = require("typeorm");
const credit_account_entity_1 = require("./entity/credit.account.entity");
const paydunya_payment_entity_1 = require("./entity/paydunya.payment.entity");
const common_2 = require("./utils/common");
let PaydunyaService = class PaydunyaService {
    constructor(creditAccountRepositpty, paydunyaPaymentRepositpty, options) {
        this.creditAccountRepositpty = creditAccountRepositpty;
        this.paydunyaPaymentRepositpty = paydunyaPaymentRepositpty;
        this.options = options;
        const setup = new paydunya.Setup({
            masterKey: this.options.PAYDUNYA_MASTER_KEY,
            privateKey: this.options.PAYDUNYA_PRIVATE_KEY,
            publicKey: this.options.PAYDUNYA_PUBLIC_KEY,
            token: this.options.PAYDUNYA_TOKEN,
            mode: this.options.PAYDUNYA_MODE,
        });
        const store = new paydunya.Store({
            name: this.options.PAYDUNYA_NAME,
        });
        this.invoice = new paydunya.OnsiteInvoice(setup, store);
        this.directPay = new paydunya.DirectPay(setup);
        this.checkout = new paydunya.CheckoutInvoice(setup, store);
    }
    addItem({ name, quantity, unitPrice, description }) {
        this.invoice.addItem(name, quantity, unitPrice, quantity * unitPrice, description);
    }
    async create({ customer, items }) {
        let totalAmount = 0;
        for (let item of items) {
            this.addItem(item);
            totalAmount += item.quantity * item.unitPrice;
        }
        this.invoice.totalAmount = totalAmount;
        await this.invoice.create(customer);
        return this.invoice;
    }
    async charge({ code, oprToken }) {
        await this.invoice.charge(oprToken, code);
        const { status, responseText, receiptURL } = this.invoice;
        return await this.paydunyaPaymentRepositpty.save({
            status,
            responseText,
            receiptURL,
        });
    }
    async checkoutInvoice(token) {
        await this.checkout.confirm(token);
        const { status } = this.checkout;
        return status;
    }
    async creditAccount({ customer, amount, }) {
        return await this.creditAccountRepositpty.save(await this.directPay.creditAccount(customer, amount));
    }
};
PaydunyaService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(credit_account_entity_1.CreditAccount)),
    __param(1, typeorm_1.InjectRepository(paydunya_payment_entity_1.PaydunyaPayment)),
    __param(2, common_1.Inject(common_2.PAYDUNYA_CONFIG)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository, Object])
], PaydunyaService);
exports.PaydunyaService = PaydunyaService;
//# sourceMappingURL=paydunya.service.js.map