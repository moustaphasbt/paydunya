import { DynamicModule } from '@nestjs/common';
import { Options } from './utils/common';
export declare class PaydunyaModule {
    static forRoot({ imports, inject, useFactory }: Options): DynamicModule;
}
