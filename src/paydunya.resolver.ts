import { Injectable } from '@nestjs/common';
import { Args, Mutation, Query } from '@nestjs/graphql';
import {
  CreateReturn,
  CreateInput,
  ChargeInput,
  CreditAccountInput,
} from './dto';
import { CreditAccount } from './entity/credit.account.entity';
import { PaydunyaPayment } from './entity/paydunya.payment.entity';
import { PaydunyaService } from './paydunya.service';

@Injectable()
export class PaydunyaResolver {
  constructor(private readonly paydunyaService: PaydunyaService) {}

  @Mutation(() => CreateReturn)
  async create(
    @Args('input', { type: () => CreateInput })
    input: CreateInput,
  ) {
    return await this.paydunyaService.create(input);
  }

  @Mutation(() => PaydunyaPayment)
  async charge(
    @Args('input', { type: () => ChargeInput })
    input: ChargeInput,
  ) {
    return await this.paydunyaService.charge(input);
  }

  @Query(() => String)
  async checkoutInvoice(@Args('token', { type: () => String }) token: string) {
    return await this.paydunyaService.checkoutInvoice(token);
  }

  @Mutation(() => CreditAccount)
  async creditAccount(
    @Args('input', { type: () => CreditAccountInput })
    input: CreditAccountInput,
  ) {
    return await this.paydunyaService.creditAccount(input);
  }
}
