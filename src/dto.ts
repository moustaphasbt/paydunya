import { Field, InputType, ObjectType } from '@nestjs/graphql';
import { IsNotEmpty, IsUUID } from 'class-validator';

@InputType()
export class ItemType {
  @Field(() => String)
  name: string;

  @Field(() => Number)
  quantity: number;

  @Field(() => Number)
  unitPrice: number;

  @Field({ nullable: true })
  description?: string;
}

@InputType()
export class CreditAccountInput {
  @Field(() => String)
  customer: string;

  @Field(() => Number)
  amount: number;
}

export class CreditAccountIdInput {
  @IsNotEmpty()
  @IsUUID()
  @Field(() => String)
  id: string;
}

export class PaydunyaPaymentIdInput {
  @IsNotEmpty()
  @IsUUID()
  @Field(() => String)
  id: string;
}

@ObjectType()
export class CreateReturn {
  @Field(() => String)
  oprToken: string;

  @Field(() => String)
  token: string;

  @Field(() => String)
  responseText: string;
}

@InputType()
export class CreateInput {
  @Field(() => String)
  customer: string;

  @Field(() => [ItemType])
  items: [ItemType];
}

@InputType()
export class ChargeInput {
  @Field(() => String)
  oprToken: string;

  @Field(() => String)
  code: string;
}
