import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as paydunya from 'paydunya';
import { Repository } from 'typeorm';
import {
  ItemType,
  CreateInput,
  CreateReturn,
  ChargeInput,
  CreditAccountInput,
} from './dto';
import { CreditAccount } from './entity/credit.account.entity';
import { PaydunyaPayment } from './entity/paydunya.payment.entity';
import { PaydunyaOptions, PAYDUNYA_CONFIG } from './utils/common';

@Injectable()
export class PaydunyaService {
  private invoice: any;
  private directPay: any;
  private checkout: any;
  constructor(
    @InjectRepository(CreditAccount)
    private creditAccountRepositpty: Repository<CreditAccount>,
    @InjectRepository(PaydunyaPayment)
    private paydunyaPaymentRepositpty: Repository<PaydunyaPayment>,
    @Inject(PAYDUNYA_CONFIG)
    private options: PaydunyaOptions,
  ) {
    const setup = new paydunya.Setup({
      masterKey: this.options.PAYDUNYA_MASTER_KEY,
      privateKey: this.options.PAYDUNYA_PRIVATE_KEY,
      publicKey: this.options.PAYDUNYA_PUBLIC_KEY,
      token: this.options.PAYDUNYA_TOKEN,
      mode: this.options.PAYDUNYA_MODE,
    });
    const store = new paydunya.Store({
      name: this.options.PAYDUNYA_NAME,
    });

    this.invoice = new paydunya.OnsiteInvoice(setup, store);
    this.directPay = new paydunya.DirectPay(setup);
    this.checkout = new paydunya.CheckoutInvoice(setup, store);
  }

  addItem({ name, quantity, unitPrice, description }: ItemType) {
    this.invoice.addItem(
      name,
      quantity,
      unitPrice,
      quantity * unitPrice,
      description,
    );
  }

  async create({ customer, items }: CreateInput): Promise<CreateReturn> {
    let totalAmount = 0;
    for (let item of items) {
      this.addItem(item);
      totalAmount += item.quantity * item.unitPrice;
    }
    this.invoice.totalAmount = totalAmount;
    await this.invoice.create(customer);
    return this.invoice;
  }

  async charge({ code, oprToken }: ChargeInput): Promise<PaydunyaPayment> {
    await this.invoice.charge(oprToken, code);
    const { status, responseText, receiptURL } = this.invoice;
    return await this.paydunyaPaymentRepositpty.save({
      status,
      responseText,
      receiptURL,
    });
  }

  async checkoutInvoice(token: string) {
    await this.checkout.confirm(token);
    const { status } = this.checkout;
    return status;
  }

  async creditAccount({
    customer,
    amount,
  }: CreditAccountInput): Promise<CreditAccount> {
    return await this.creditAccountRepositpty.save(
      await this.directPay.creditAccount(customer, amount),
    );
  }
}
