import { DynamicModule, Module } from '@nestjs/common';
import { PaydunyaResolver } from './paydunya.resolver';
import { PaydunyaService } from './paydunya.service';
import { Options, PAYDUNYA_CONFIG } from './utils/common';

@Module({})
export class PaydunyaModule {
  static forRoot({ imports, inject, useFactory }: Options): DynamicModule {
    return {
      imports: imports,
      module: PaydunyaModule,
      providers: [
        PaydunyaResolver,
        PaydunyaService,
        {
          provide: PAYDUNYA_CONFIG,
          useFactory,
          inject,
        },
      ],
      exports: [PaydunyaService],
    };
  }
}
